﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B_trees
{
    public class Tree234
    {
        public const int MaxLevel = 4;

        private Tree234Node root;
        private bool isNull;
        private int level;
        private Frame curFrame;
        private MainForm mainForm;

        public Tree234Node Root
        { get { return root; } }
        
        public bool IsEmpty { get { return isNull; } }    
        public int Lavel { get { return level; } }

        public Tree234(MainForm mf)
        {
            isNull = true;
            level = 1;
            curFrame = null;
            mainForm = mf;
        }

        public void Insert(KeyValue<long, string> pair)
        {
            Tree234Node current;

            if (isNull)
            {
                root = new Tree234Node(pair);
                current = root;

                isNull = false;

                curFrame = CreateFrame("Вставка " + pair.Key, "Створення дерева");
            }
            else
            {
                current = root;

                while (true)
                {
                    curFrame = CreateFrame("Вставка " + pair.Key, "Обераємо вузол " + current.ToString());

                    PainFrame(current);

                    if (current.IsFull)
                    {
                        curFrame.AdditionalText += ". Вузол повний, виконаємо розбиття";
                        mainForm.AddFrame(curFrame);

                        Tree234Node t = Split(current);

                        if (t != null)
                        {
                            curFrame = CreateFrame("Вставка " + pair.Key, "Обраний елемент " + current.ToString());

                            PainFrame(current);

                            current = GetNextChild(current.Parent, pair.Key);

                            curFrame.AdditionalText += ". Переходимо до вузла " + current.ToString();
                        }
                        else
                        {
                            curFrame = CreateFrame("Вставка " + pair.Key, "Досягнуто максимольної висоти дерева. Не можливо виконати вставку.");
                            curFrame.Nodes[0].ForeColor = Color.Red;
                            curFrame.Type = FrameType.Primary;
                            mainForm.AddFrame(curFrame);

                            return;
                        }
                    }
                    else if (current.IsLeaf)
                    {
                        curFrame.AdditionalText += ". Досягнуто листа";
                        mainForm.AddFrame(curFrame);
                        break;
                    }
                    else
                    {
                        current = GetNextChild(current, pair.Key);
                        curFrame.AdditionalText += ". Переходимо до вузла " + current.ToString();
                        mainForm.AddFrame(curFrame);
                    }
                }

                current.InsertItem(pair);

                curFrame = CreateFrame("Вставка " + pair.Key, "Ключ вставлено");
                PainFrame(current);
            }

            for (int i = 0; i < 3; i++)
			{
                if (curFrame.Nodes[current.Number].Pairs[i] != null && curFrame.Nodes[current.Number].Pairs[i].Key == pair.Key)
                {
                    curFrame.Nodes[current.Number].ItemsColor[i] = Color.Orange;
                    curFrame.Nodes[current.Number].NewBackground();
                }
			}

            curFrame.Type = FrameType.Primary;
            mainForm.AddFrame(curFrame);
            curFrame = null;
        }

        private void PainFrame(Tree234Node current)
        {
            int p;

            if (current != root)
            {
                curFrame.Nodes[current.Number].ForeColor = Color.YellowGreen;

                //фарбуємо лянцюг до елементу
                p = (current.Number - 1) / 4;
                while (p > 0)
                {
                    curFrame.Nodes[p].ForeColor = Color.Green;
                    p = (p - 1) / 4;
                }
            }
        }

        private Tree234Node GetNextChild(Tree234Node node, long key)
        {
            int i;
            int n = node.Count;

            for (i = 0; i < n; i++)
            {
                if (key < node.GetKeyValuePair(i).Key)
                    return node[i];
            }

            return node[i];
        }

        private Tree234Node GetNextChild(Tree234Node node, long key, ref int index)
        {
            int i;
            int n = node.Count;

            for (i = 0; i < n; i++)
            {
                if (key < node.GetKeyValuePair(i).Key)
                {
                    index = i;
                    return node[i];
                }
            }

            index = i;
            return node[i];
        }

        private Tree234Node Split(Tree234Node node)
        {
            if (level == MaxLevel && node == root)
            {
                System.Windows.Forms.MessageBox.Show("Досянуто максимального рівня дерева: " + MaxLevel.ToString() + ". Вставка нових елементів неможлива.", "Заповнення");
                return null;
            }

            int itemIndex;

            KeyValue<long, string>  itemC = node.RemoveItem(), 
                                    itemB = node.RemoveItem();

            Tree234Node parent,
                child2 = node.DisconnectChild(2),
                child3 = node.DisconnectChild(3);

            Tree234Node newRight = new Tree234Node();

            if (node == root)
            {
                root = new Tree234Node();
                parent = root;
                root.ConnectChild(0, node);
                level++;
            }
            else
                parent = node.Parent;

            itemIndex = parent.InsertItem(itemB);
            int n = parent.Count;

            for (int i = n - 1; i > itemIndex; i--)
            {
                Tree234Node temp = parent.DisconnectChild(i);
                parent.ConnectChild(i + 1, temp);
            }

            parent.ConnectChild(itemIndex + 1, newRight);

            newRight.InsertItem(itemC);
            newRight.ConnectChild(0, child2);
            newRight.ConnectChild(1, child3);

            return newRight;
        }

        public KeyValue<long, string> Find(long key)
        {
            if (isNull)
                return null;

            Tree234Node current = root;

            int curChild;

            while (true) 
            {
                curFrame = CreateFrame("Пошук " + key, "Шукаємо в вузлі " + current.ToString());

                //підфарбовуємо поточний вузол
                if (current != root)
                {
                    curFrame.Nodes[current.Number].ForeColor = Color.YellowGreen;

                    //фарбуємо лянцюг до елементу
                    int p = (current.Number - 1) / 4;
                    while (p > 0)
                    {
                        curFrame.Nodes[p].ForeColor = Color.Green;
                        p = (p - 1) / 4;
                    }
                }

                //якщо знайшли шуканий ключ
                if ((curChild = current.FindItem(key)) != -1)
                {
                    curFrame.Nodes[current.Number].ItemsColor[curChild] = Color.Orange;
                    curFrame.Nodes[current.Number].NewBackground();
                    curFrame.AdditionalText += ". Ключ знайдено";
                    curFrame.Type = FrameType.Primary;
                    mainForm.AddFrame(curFrame);

                    return current.Pairs[curChild];
                }
                //якщо ключа нема, а це цист
                else if (current.IsLeaf)
                {
                    curFrame.Nodes[current.Number].ForeColor = Color.Red;

                    curFrame.AdditionalText += ". В листовому вузлі ключ не знайдено, отже в дереві він відсутній";
                    curFrame.Type = FrameType.Primary;
                    mainForm.AddFrame(curFrame);

                    return null;
                }
                else
                {
                    current = GetNextChild(current, key);

                    curFrame.AdditionalText += ". Ключ не знайдемо, переходимо до нащадка " + current.ToString();
                    mainForm.AddFrame(curFrame);
                }
            }
        }

        public bool QuietFind(long key)
        {
            if (isNull)
                return false;

            Tree234Node current = root;

            while (true)
            {
                if (current.FindItem(key) != -1)
                {
                    return true;
                }
                else if (current.IsLeaf)
                {
                    return false;
                }
                else
                    current = GetNextChild(current, key);
            }
        }

        public bool Delete(long key) 
        {
            if (isNull)
            {
                //дерево пусте нема що видаляти
                curFrame = CreateFrame("Видалення " + key, "Дерево пусте, не можливо видалити елемент");
                curFrame.Type = FrameType.Primary;
                mainForm.AddFrame(curFrame);
                curFrame = null;
                
                return false;
            }

            bool output;

            //обраний вузол
            Tree234Node current = root;

            //від'ємна лише для root-а,
            //для решти повинна мати нормальне значення
            int currentChildrenIndex = -1;
            //допоміжне сховище індексу
            int index = -1;

            //ключ який буде шукатися
            long findKey = key;
            //вузол, де знайдено ключ
            Tree234Node findedNode = null; 

            while (true) 
            {
                //-----позбуваємось 2-вузлів(вузлів з ОДНИМ ключем)-----
                curFrame = CreateFrame("Видалення " + key, "Обераємо вузол " + current.ToString());
                PainFrame(current);

                //якщо знайшли 2-вузол
                if (current.Count == 1) 
                {
                    //якщо це корінь, перевіремо чи не є обидва його нащадки 2-вузлами
                    if (current == root)
                    {
                        //примітка: якщо існує 0-вий нащадок, то точно існує 1-ший
                        if (root[0] != null && root[0].Count == 1 && root[1].Count == 1)
                        {
                            curFrame.AdditionalText += ". Виконуємо злиття кореня";
                            mainForm.AddFrame(curFrame);
                            
                            //виконуємо злиття кореню
                            DoFusionRoot();
                            
                            curFrame = CreateFrame("Видалення " + key, "Обрано вузол " + current.ToString());
                        }

                        //іншихва ріантів для кореню не передбачено
                    }
                    else 
                    {
                        //ми не в корені, перевіремо можливість лівого/правого повороту чи злиття

                        //правий поворот
                        //зправа має бути брат з >1 ключем
                        if (currentChildrenIndex < 3 && //чи ми можемо мати брата зправа
                            current.Parent[currentChildrenIndex + 1] != null && //чи він існує
                            current.Parent[currentChildrenIndex + 1].Count > 1) //чи він має більше 1 ключа
                        {
                            //ніштяк! виконуємо правий поворот
                            DoRightRotation(current, currentChildrenIndex);

                            curFrame.AdditionalText += ". Виконуємо правий поворот";
                        }
                        //лівий поворот
                        else if (currentChildrenIndex > 0 && //чи ми можемо мати брата зліва, якщо так -- він точно існує
                                 current.Parent[currentChildrenIndex - 1].Count > 1) //чи він має більше 1 ключа
                        {
                            //ніштяк! виконуємо лівий поворот
                            DoLeftRotation(current, currentChildrenIndex);

                            curFrame.AdditionalText += ". Виконуємо лівий поворот";
                        }
                        //злиття
                        //ні зправа, ні зліва нема >2-вузлів, а самі ми бути не можемо
                        //визначемо де є кандидат на злиття
                        else if (currentChildrenIndex < 3 && //може бути брат зправа
                                 current.Parent[currentChildrenIndex + 1] != null) //і він існує
                        {
                            //ніштяк! виконуємо злиття з правим 2-братом
                            DoFusionWithRigth(current, currentChildrenIndex);

                            curFrame.AdditionalText += ". Виконуємо злиття з правим";
                        }
                        else if (currentChildrenIndex > 0) // ми маємо 2-брата зліва
                        {
                            //ніштяк! виконуємо злиття з лівим 2-братом
                            //для чого зливати з лівим, якщо можна трохи помахлювати...
                            current = current.Parent[currentChildrenIndex - 1];
                            //і злити з правим
                            DoFusionWithRigth(current , currentChildrenIndex - 1);
                            
                            curFrame.AdditionalText += ". Виконуємо злиття з лівим";
                        }
                        else
                        {
                            throw new InvalidOperationException("Цього не мало статися. Не можливо вирішити 2-вузол.");
                        }

                        mainForm.AddFrame(curFrame);
                        curFrame = CreateFrame("Видалення " + key, "Обрано вузол " + current.ToString());
                        PainFrame(current);
                    }
                }

                //ми точно не в 2-вузлі(або це корінь)

                //шукаємо ключ, якщо знайшли...
                if ((index = current.FindItem(findKey)) >= 0) 
                {
                    //якщо ми знайшли шуканий ключ
                    curFrame.AdditionalText += ". Знайдено ключ " + findKey;

                    //ми перший раз знайшли ключ і це не лист
                    if (findKey == key && !current.IsLeaf)
                    {
                        //запам'ятовуємо де ми знайшли ключ;
                        findedNode = current;
                        findedNode.Pairs[index].Key = -1;

                        //новий шуканий ключ рівний найменшому ключу, більшому за даний;
                        findKey = FindMinHigher(current[index + 1]);

                        curFrame.AdditionalText += ", позначимо його. Продовжуємо пошук ключа " + findKey; 
                    }
                    else 
                    {
                        //просто видаляємо його геть
                        KeyValue<long, string> d = current.RemovePair(index);


                        if (findedNode != null)
                        {
                            curFrame.AdditionalText += ". Замінимо позначений елемент на знайдений";

                            ReplaceMarkedKey(findedNode, d);
                        }
                        else 
                        {
                            curFrame.AdditionalText += ". Видаляємо шуканий елемент";
                        }


                        mainForm.AddFrame(curFrame);

                        if (root.Count == 0)
                        {
                            isNull = true;
                            root = null;

                            curFrame = CreateFrame("Видалення " + key, "Видалено останній елемент. Дерево пусте.");
                        }
                        else 
                        {
                            curFrame = CreateFrame("Видалення " + key, "");
                            PainFrame(current);
                        }

                        output = true;

                        break;
                    }
                }

                if (!current.IsLeaf)
                {
                    current = GetNextChild(current, findKey, ref currentChildrenIndex);

                    curFrame.AdditionalText += ". Переходимо до вузла " + current.ToString();
                }
                else 
                {
                    //елемента в дереві немає, не можливо видалити
                    curFrame = CreateFrame("Видалення " + key, "Ключ не знайдено");

                    PainFrame(current);
                    curFrame.Nodes[current.Number].ForeColor = Color.Red;

                    output = false;
                    break;
                }

                mainForm.AddFrame(curFrame);
            }

            //виведення того, що ми наробили
            curFrame.Type = FrameType.Primary;
            mainForm.AddFrame(curFrame);
            curFrame = null;

            return output;
        }

        private bool ReplaceMarkedKey(Tree234Node node, KeyValue<long, string> pair)
        {
            int index = node.FindItem(-1);

            if (index != -1)
            {
                node.Pairs[index] = pair;
                return true;
            }
            else
                for (int i = 0; i < 4; i++)
                {
                    if (node[i] == null)
                        break;
                    else if (ReplaceMarkedKey(node[i], pair))
                        return true;
                }

            return false;
        }

        private long FindMinHigher(Tree234Node node)
        {
            if (node.IsLeaf)
            {
                return node.Pairs[0].Key;
            }
            else
                return FindMinHigher(node[0]);
        }

        private void DoFusionWithRigth(Tree234Node current, int index)
        {
            Tree234Node rightSibling = current.Parent[index + 1], parent = current.Parent;

            //переміщення ключів
            current.Pairs[1] = parent.Pairs[index];
            current.Pairs[2] = rightSibling.Pairs[0];


            //перевішування нащадків на нові місця
            Tree234Node t = rightSibling.DisconnectChild(0);
            current.ConnectChild(2, t);
            t = rightSibling.DisconnectChild(1);
            current.ConnectChild(3, t);

            //зсув елементів батька вліво 
            int pcount = parent.Count, i;
            for (i = index + 1; i < pcount; i++)
            {
                parent.Pairs[i - 1] = parent.Pairs[i];
                t = parent.DisconnectChild(i + 1);
                parent.ConnectChild(i, t);
            }
            parent.Pairs[i - 1] = null;
            parent.DisconnectChild(i);
        }

        private void DoLeftRotation(Tree234Node current, int index)
        {
            Tree234Node leftSibling = current.Parent[index - 1], parent = current.Parent;

            //зсуваємо всіх вліво на одну позицію
            current.Pairs[1] = current.Pairs[0];
            //перевішуємо більшого нащадка
            Tree234Node t = current.DisconnectChild(1);
            current.ConnectChild(2, t);
            //перевішуємо молодшого нащадка
            t = current.DisconnectChild(0);
            current.ConnectChild(1, t);

            //спускаємо вниз батьківський ключ
            current.Pairs[0] = parent.Pairs[index - 1];

            //піднімаємо в батька ключ брата
            int lcount = leftSibling.Count;
            parent.Pairs[index - 1] = leftSibling.Pairs[lcount - 1];
            leftSibling.Pairs[lcount - 1] = null;

            //перевісимо найстаршого нащадка брата
            t = leftSibling.DisconnectChild(lcount);
            current.ConnectChild(0, t);
        }

        private void DoRightRotation(Tree234Node current, int index)
        {
            Tree234Node rightSibling = current.Parent[index + 1], parent = current.Parent;

            //спускаємо батьківський ключ вниз
            current.Pairs[1] = parent.Pairs[index];

            //виштовхуємо менший елемент брата в батька
            parent.Pairs[index] = rightSibling.Pairs[0];

            //перевішуємо молодшого нащадка брата старшим нащадком собі
            Tree234Node zeroNode = rightSibling.DisconnectChild(0);
            current.ConnectChild(2, zeroNode);

            //посуваємо вліво все у брата
            int count = rightSibling.Count, i;
            Tree234Node t;
            for(i = 1; i < count; i++)
            {
                rightSibling.Pairs[i - 1] = rightSibling.Pairs[i];
                t = rightSibling.DisconnectChild(i);
                rightSibling.ConnectChild(i - 1, t);
            }
            //затираємо октанній ключ
            rightSibling.Pairs[i - 1] = null;
            //переносимо більшого нащадка
            t = rightSibling.DisconnectChild(i);
            rightSibling.ConnectChild(i - 1, t);
        }

        private void DoFusionRoot()
        {
            Tree234Node Left = root.Children[0], Right = root.Children[1];

            root.Pairs[1] = root.Pairs[0];
            root.Pairs[0] = Left.RemoveItem();
            root.Pairs[2] = Right.RemoveItem();

            root.ConnectChild(0, Left.DisconnectChild(0));
            root.ConnectChild(1, Left.DisconnectChild(1));
            root.ConnectChild(2, Right.DisconnectChild(0));
            root.ConnectChild(3, Right.DisconnectChild(1));

            level--;
        }

        public Frame CreateFrame(string name, string additionalText)
        {
            Frame frame = new Frame();
            frame.Text = name;
            frame.AdditionalText = additionalText;
            frame.Level = level;
            
            frame.Nodes[0] = new Node(root, System.Drawing.Color.Blue);

            AddNodeInFrame(frame, root);

            return frame;
        }

        private void AddNodeInFrame(Frame frame, Tree234Node node)
        {
            if (node != null && !node.IsLeaf)
                for (int i = 0; i < 4; i++)
                    if (node.Children[i] != null)
                    {
                        frame.Nodes[node.Number].Children[i] = node.Children[i].Number;
                        frame.Nodes[node.Children[i].Number] = new Node(node.Children[i]);
                        AddNodeInFrame(frame, node.Children[i]);
                    }
                    else
                    {
                        break;
                    }
        }
    }
}
