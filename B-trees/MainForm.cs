﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace B_trees
{
    public partial class MainForm : Form
    {
        Tree234 tree;
        Random r;
        Frame currentFrame;
        bool played;
        List<long> pasted;

        public MainForm()
        {
            r = new Random();

            pasted = new List<long>(20);

            played = false;

            InitializeComponent();

            DoubleBuffered = true;

            int t = r.Next() % 1000;
            pasted.Add(t);
            tree = new Tree234(this);
            tree.Insert(new KeyValue<long, string>(t, GetRandomName()));
        }

        private void проПрограмуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #region Draw

        public void AddFrame(object obj)
        {
            Frame frame = (Frame)obj;

            framesFlowLayoutPanel.Controls.Add(frame);

            if (framesFlowLayoutPanel.Controls.Count == 1 ||
               (currentFrame != null &&
               framesFlowLayoutPanel.Controls.IndexOf(currentFrame) == framesFlowLayoutPanel.Controls.Count - 2 &&
               currentFrame.Type == FrameType.Primary &&
               frame.Type == FrameType.Intermediate))
            {
                if (currentFrame != null)
                    currentFrame.Enable = false;

                currentFrame = frame;
                currentFrame.Enable = true;
                DrawCurrentFrame();
            }

            framesFlowLayoutPanel.ScrollControlIntoView(currentFrame);
        }

        public void ShowFrame(object obj)
        {
            Frame frame = (Frame)obj;

            if (currentFrame != null)
                currentFrame.Enable = false;

            currentFrame = frame;
            currentFrame.Enable = true;
            framesFlowLayoutPanel.ScrollControlIntoView(currentFrame);

            DrawCurrentFrame();
        }

        private void DrawCurrentFrame()
        {
            workPanel.Controls.Clear();

            if (currentFrame == null)
                return;

            mainActionLabel.Text = currentFrame.Text;

            float size = 12F;
            Graphics g = this.CreateGraphics();
            System.Drawing.Font f = new Font("Microsoft Sans Serif", size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
            while (g.MeasureString(currentFrame.AdditionalText, f).Width > workPanel.Width) 
            {
                size -= 0.5F;
                f = new Font("Microsoft Sans Serif", size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
            }

            additionalDescriptionLabel.Font = f;
            additionalDescriptionLabel.Text = currentFrame.AdditionalText;

            int maxX = 0, maxY = 0;

            int nodeWidth = trackBar1.Value, nodeHeigth = (int)(trackBar1.Value * 0.3);

            for (int i = 0; i < currentFrame.Nodes.Length; i++)
            {
                if (currentFrame.Nodes[i] != null)
                    currentFrame.Nodes[i].Size = new Size(nodeWidth, nodeHeigth);
            }

            int stepX = nodeWidth + 20, stepY = nodeHeigth * 2, marginYBig = nodeHeigth + 15, marginYSmall = 10;

            for (int i = currentFrame.Level - 1; i >= 0; i--)
            {
                int start = (int)Math.Pow(4, i) / 3,
                    end = (int)Math.Pow(4, i + 1) / 3;


                int pos = 0, j;

                if (i == Tree234.MaxLevel - 1)
                {
                    int n = 0;

                    for (j = start; j < end; j++)
                        if (currentFrame.Nodes[j] != null)
                        {
                            if (n % 2 == 0)
                                currentFrame.Nodes[j].Top = (currentFrame.Nodes[j].Level - 1) * stepY + marginYBig;
                            else
                                currentFrame.Nodes[j].Top = (currentFrame.Nodes[j].Level - 1) * stepY + marginYSmall;

                            n++;

                            currentFrame.Nodes[j].Left = pos;

                            if ((pos + currentFrame.Nodes[j].Width) > maxX)
                                maxX = pos + currentFrame.Nodes[j].Width;

                            if ((currentFrame.Nodes[j].Top + currentFrame.Nodes[j].Height) > maxY)
                                maxY = currentFrame.Nodes[j].Top + currentFrame.Nodes[j].Height;

                            pos += stepX / 2;
                        }
                }
                else
                {
                    for (j = start; j < end; j++)
                    {
                        if (currentFrame.Nodes[j] != null)
                        {
                            int n, poss = 0;
                            for (n = 0; n < 4; n++)
                                if (currentFrame.Nodes[j].Children[n] != -1)
                                    poss += currentFrame.Nodes[currentFrame.Nodes[j].Children[n]].Left;
                                else
                                    break;

                            if (n != 0)
                                pos = poss / n;

                            currentFrame.Nodes[j].Left = pos;

                            if ((pos + currentFrame.Nodes[j].Width) > maxX)
                                maxX = pos + currentFrame.Nodes[j].Width;

                            pos += stepX + (2 - i) * 20;
                            currentFrame.Nodes[j].Top = (currentFrame.Nodes[j].Level - 1) * stepY + marginYSmall;

                            if ((currentFrame.Nodes[j].Top + currentFrame.Nodes[j].Height) > maxY)
                                maxY = currentFrame.Nodes[j].Top + currentFrame.Nodes[j].Height;
                        }
                    }
                }
            }

            int deltaX = (workPanel.Width - maxX) / 2,
                deltaY = (workPanel.Height - maxY) / 2;

            for (int i = currentFrame.Nodes.Length - 1; i >= 0; i--)
                if (currentFrame.Nodes[i] != null)
                {
                    if(deltaX > 0)
                        currentFrame.Nodes[i].Left += deltaX;

                    if (deltaY > 0)
                        currentFrame.Nodes[i].Top += deltaY;
                }

            workPanel.Controls.AddRange(currentFrame.Nodes);
        }

        private void DrawLines()
        {
            Graphics g = workPanel.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.Clear(workPanel.BackColor);

            for (int i = currentFrame.Nodes.Length - 1; i >= 0; i--)
                if (currentFrame.Nodes[i] != null)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        if (currentFrame.Nodes[i].Children[j] > 0)
                        {
                            g.DrawLine(currentFrame.Nodes[currentFrame.Nodes[i].Children[j]].ForegrountPen,
                                        currentFrame.Nodes[i].ChildrenPoints[j],
                                        currentFrame.Nodes[currentFrame.Nodes[i].Children[j]].MidPoint);
                        }
                        else
                            break;
                    }
                }
        }

        private void workPanel_Resize(object sender, EventArgs e)
        {
            DrawCurrentFrame();
        }
        
        private void workPanel_Paint(object sender, PaintEventArgs e)
        {
            DrawLines();
        }

        #endregion 

        #region Actions

        private void addRandomElemButton_Click(object sender, EventArgs e)
        {
            int t;
            do
            {
                t = r.Next() % 1000;
            }
            while (tree.QuietFind(t));

            pasted.Add(t);
            tree.Insert(new KeyValue<long, string>(t, GetRandomName()));
        }

        private void addElemButton_Click(object sender, EventArgs e)
        {
            int t;

            if (!Int32.TryParse(textBox.Text, out t))
                return;

            if (!tree.QuietFind(t))
            {
                pasted.Add(t);
                tree.Insert(new KeyValue<long, string>(t, GetRandomName()));
            }
            else
            {
                MessageBox.Show("Такий елемент вже існує. Вставка не можлива.", "Вставка " + t);
            }

            textBox.Text = "";
        }

        private string GetRandomName() 
        {
            return  global::B_trees.Properties.Settings.Default.FirstNames[r.Next(19)] + " " +
                    global::B_trees.Properties.Settings.Default.LastNames[r.Next(59)];
        }

        private void findRandomElemButton_Click(object sender, EventArgs e)
        {
            if (pasted.Count < 1 || ((r.Next() % 1000) < 100))
                tree.Find(r.Next() % 1000);
            else
                tree.Find(pasted[r.Next(pasted.Count)]);
        }

        private void findElemButton_Click(object sender, EventArgs e)
        {
            int t;

            if (!Int32.TryParse(textBox.Text, out t))
                return;

            tree.Find(t);

            textBox.Text = "";
        }

        private void delRandomElemButton_Click(object sender, EventArgs e)
        {
            if (pasted.Count == 0)
                return;

            int t;
            long tt;

            if ((r.Next() % 1000) < 200)
            {
                tt = r.Next() % 1000;
            }
            else
            {
                t = r.Next(pasted.Count);
                tt = pasted[t];
            }

            if (tree.Delete(tt))
                pasted.Remove(tt);
        }

        private void delElemВutton_Click(object sender, EventArgs e)
        {
            int t;

            if (!Int32.TryParse(textBox.Text, out t))
                return;

            if (tree.Delete(t))
                pasted.Remove(t);

            textBox.Text = "";
        }

        #endregion

        #region MovieButtons

        private void playButton_Click(object sender, EventArgs e)
        {
            played = !played;

            timer.Enabled = played;

            if (played)
                playButton.BackgroundImage = global::B_trees.Properties.Resources.player_pause;
            else
                playButton.BackgroundImage = global::B_trees.Properties.Resources.player_play;

        }

        private void forwardButton_Click(object sender, EventArgs e)
        {
            if (currentFrame != null && framesFlowLayoutPanel.Controls.Count != 0)
            {
                int i = framesFlowLayoutPanel.Controls.IndexOf(currentFrame);

                if (i < framesFlowLayoutPanel.Controls.Count - 1)
                    ShowFrame(framesFlowLayoutPanel.Controls[i + 1]);
            }
        }

        private void toEndButton_Click(object sender, EventArgs e)
        {
            if (framesFlowLayoutPanel.Controls.Count != 0) 
            {
                ShowFrame(framesFlowLayoutPanel.Controls[framesFlowLayoutPanel.Controls.Count - 1]);
            }
        }

        private void rewindButton_Click(object sender, EventArgs e)
        {
            if (currentFrame != null && framesFlowLayoutPanel.Controls.Count != 0)
            {
                int i = framesFlowLayoutPanel.Controls.IndexOf(currentFrame);

                if (i > 0)
                    ShowFrame(framesFlowLayoutPanel.Controls[i - 1]);
            }
        }

        private void toStartButton_Click(object sender, EventArgs e)
        {
            if (framesFlowLayoutPanel.Controls.Count != 0)
            {
               ShowFrame(framesFlowLayoutPanel.Controls[0]);
            }
        }

        private void navigationGroupBox_Resize(object sender, EventArgs e)
        {
            int w = navigationGroupBox.Width / 2;

            playButton.Left = w - 14;
            rewindButton.Left = w - 48;
            forwardButton.Left = w + 20;
            toEndButton.Left = w + 54;
            toStartButton.Left = w - 82;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (framesFlowLayoutPanel.Controls[framesFlowLayoutPanel.Controls.Count - 1] != currentFrame)
                ShowFrame(framesFlowLayoutPanel.Controls[framesFlowLayoutPanel.Controls.IndexOf(currentFrame) + 1]);
            else
            {
                played = false;

                timer.Enabled = false;

                playButton.BackgroundImage = global::B_trees.Properties.Resources.player_play;
            }
        }

        #endregion

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar));
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            DrawCurrentFrame();
        }


    }
}
