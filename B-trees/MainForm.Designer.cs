﻿namespace B_trees
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.вихідToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.допомогаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проПрограмуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRandomElemButton = new System.Windows.Forms.Button();
            this.actionGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.textBox = new System.Windows.Forms.TextBox();
            this.delRandomElemButton = new System.Windows.Forms.Button();
            this.delElemВutton = new System.Windows.Forms.Button();
            this.findElemButton = new System.Windows.Forms.Button();
            this.findRandomElemButton = new System.Windows.Forms.Button();
            this.addElemButton = new System.Windows.Forms.Button();
            this.navigationGroupBox = new System.Windows.Forms.GroupBox();
            this.toEndButton = new System.Windows.Forms.Button();
            this.rewindButton = new System.Windows.Forms.Button();
            this.framesFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.forwardButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.toStartButton = new System.Windows.Forms.Button();
            this.workPanel = new System.Windows.Forms.Panel();
            this.additionalDescriptionLabel = new System.Windows.Forms.Label();
            this.mainActionLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.actionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.navigationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.допомогаToolStripMenuItem,
            this.проПрограмуToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.вихідToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(99, 6);
            // 
            // вихідToolStripMenuItem
            // 
            this.вихідToolStripMenuItem.Name = "вихідToolStripMenuItem";
            this.вихідToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.вихідToolStripMenuItem.Text = "Вихід";
            this.вихідToolStripMenuItem.Click += new System.EventHandler(this.вихідToolStripMenuItem_Click);
            // 
            // допомогаToolStripMenuItem
            // 
            this.допомогаToolStripMenuItem.Name = "допомогаToolStripMenuItem";
            this.допомогаToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.допомогаToolStripMenuItem.Text = "Допомога";
            this.допомогаToolStripMenuItem.Visible = false;
            // 
            // проПрограмуToolStripMenuItem
            // 
            this.проПрограмуToolStripMenuItem.Name = "проПрограмуToolStripMenuItem";
            this.проПрограмуToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.проПрограмуToolStripMenuItem.Text = "Про програму";
            this.проПрограмуToolStripMenuItem.Click += new System.EventHandler(this.проПрограмуToolStripMenuItem_Click);
            // 
            // addRandomElemButton
            // 
            this.addRandomElemButton.Location = new System.Drawing.Point(6, 45);
            this.addRandomElemButton.Name = "addRandomElemButton";
            this.addRandomElemButton.Size = new System.Drawing.Size(88, 50);
            this.addRandomElemButton.TabIndex = 1;
            this.addRandomElemButton.Text = "Додати випадковий елемент";
            this.addRandomElemButton.UseVisualStyleBackColor = true;
            this.addRandomElemButton.Click += new System.EventHandler(this.addRandomElemButton_Click);
            // 
            // actionGroupBox
            // 
            this.actionGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.actionGroupBox.Controls.Add(this.label1);
            this.actionGroupBox.Controls.Add(this.trackBar1);
            this.actionGroupBox.Controls.Add(this.textBox);
            this.actionGroupBox.Controls.Add(this.delRandomElemButton);
            this.actionGroupBox.Controls.Add(this.delElemВutton);
            this.actionGroupBox.Controls.Add(this.findElemButton);
            this.actionGroupBox.Controls.Add(this.findRandomElemButton);
            this.actionGroupBox.Controls.Add(this.addElemButton);
            this.actionGroupBox.Controls.Add(this.addRandomElemButton);
            this.actionGroupBox.Location = new System.Drawing.Point(5, 25);
            this.actionGroupBox.Name = "actionGroupBox";
            this.actionGroupBox.Size = new System.Drawing.Size(100, 430);
            this.actionGroupBox.TabIndex = 2;
            this.actionGroupBox.TabStop = false;
            this.actionGroupBox.Text = "Дії:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 411);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Масштаб";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(6, 384);
            this.trackBar1.Maximum = 500;
            this.trackBar1.Minimum = 50;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(88, 45);
            this.trackBar1.SmallChange = 10;
            this.trackBar1.TabIndex = 9;
            this.trackBar1.TickFrequency = 50;
            this.trackBar1.Value = 100;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(6, 19);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(88, 20);
            this.textBox.TabIndex = 8;
            this.textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // delRandomElemButton
            // 
            this.delRandomElemButton.Location = new System.Drawing.Point(6, 272);
            this.delRandomElemButton.Name = "delRandomElemButton";
            this.delRandomElemButton.Size = new System.Drawing.Size(88, 50);
            this.delRandomElemButton.TabIndex = 7;
            this.delRandomElemButton.Text = "Видалити випадковий елемент";
            this.delRandomElemButton.UseVisualStyleBackColor = true;
            this.delRandomElemButton.Click += new System.EventHandler(this.delRandomElemButton_Click);
            // 
            // delElemВutton
            // 
            this.delElemВutton.Location = new System.Drawing.Point(6, 328);
            this.delElemВutton.Name = "delElemВutton";
            this.delElemВutton.Size = new System.Drawing.Size(88, 50);
            this.delElemВutton.TabIndex = 5;
            this.delElemВutton.Text = "Видалити елемент...";
            this.delElemВutton.UseVisualStyleBackColor = true;
            this.delElemВutton.Click += new System.EventHandler(this.delElemВutton_Click);
            // 
            // findElemButton
            // 
            this.findElemButton.Location = new System.Drawing.Point(6, 216);
            this.findElemButton.Name = "findElemButton";
            this.findElemButton.Size = new System.Drawing.Size(88, 50);
            this.findElemButton.TabIndex = 4;
            this.findElemButton.Text = "Знайти елемент...";
            this.findElemButton.UseVisualStyleBackColor = true;
            this.findElemButton.Click += new System.EventHandler(this.findElemButton_Click);
            // 
            // findRandomElemButton
            // 
            this.findRandomElemButton.Location = new System.Drawing.Point(6, 157);
            this.findRandomElemButton.Name = "findRandomElemButton";
            this.findRandomElemButton.Size = new System.Drawing.Size(88, 50);
            this.findRandomElemButton.TabIndex = 3;
            this.findRandomElemButton.Text = "Знайти випадковий елемент";
            this.findRandomElemButton.UseVisualStyleBackColor = true;
            this.findRandomElemButton.Click += new System.EventHandler(this.findRandomElemButton_Click);
            // 
            // addElemButton
            // 
            this.addElemButton.Location = new System.Drawing.Point(6, 101);
            this.addElemButton.Name = "addElemButton";
            this.addElemButton.Size = new System.Drawing.Size(88, 50);
            this.addElemButton.TabIndex = 2;
            this.addElemButton.Text = "Додати елемент...";
            this.addElemButton.UseVisualStyleBackColor = true;
            this.addElemButton.Click += new System.EventHandler(this.addElemButton_Click);
            // 
            // navigationGroupBox
            // 
            this.navigationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.navigationGroupBox.Controls.Add(this.toEndButton);
            this.navigationGroupBox.Controls.Add(this.rewindButton);
            this.navigationGroupBox.Controls.Add(this.framesFlowLayoutPanel);
            this.navigationGroupBox.Controls.Add(this.forwardButton);
            this.navigationGroupBox.Controls.Add(this.playButton);
            this.navigationGroupBox.Controls.Add(this.toStartButton);
            this.navigationGroupBox.Location = new System.Drawing.Point(111, 342);
            this.navigationGroupBox.Name = "navigationGroupBox";
            this.navigationGroupBox.Size = new System.Drawing.Size(565, 113);
            this.navigationGroupBox.TabIndex = 3;
            this.navigationGroupBox.TabStop = false;
            this.navigationGroupBox.Text = "Навігація:";
            this.navigationGroupBox.Resize += new System.EventHandler(this.navigationGroupBox_Resize);
            // 
            // toEndButton
            // 
            this.toEndButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.toEndButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.toEndButton.BackgroundImage = global::B_trees.Properties.Resources.player_end;
            this.toEndButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toEndButton.Location = new System.Drawing.Point(336, 13);
            this.toEndButton.Name = "toEndButton";
            this.toEndButton.Size = new System.Drawing.Size(28, 28);
            this.toEndButton.TabIndex = 3;
            this.toEndButton.UseVisualStyleBackColor = true;
            this.toEndButton.Click += new System.EventHandler(this.toEndButton_Click);
            // 
            // rewindButton
            // 
            this.rewindButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rewindButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rewindButton.BackgroundImage = global::B_trees.Properties.Resources.player_rev;
            this.rewindButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rewindButton.Location = new System.Drawing.Point(234, 13);
            this.rewindButton.Name = "rewindButton";
            this.rewindButton.Size = new System.Drawing.Size(28, 28);
            this.rewindButton.TabIndex = 4;
            this.rewindButton.UseVisualStyleBackColor = true;
            this.rewindButton.Click += new System.EventHandler(this.rewindButton_Click);
            // 
            // framesFlowLayoutPanel
            // 
            this.framesFlowLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.framesFlowLayoutPanel.AutoScroll = true;
            this.framesFlowLayoutPanel.BackColor = System.Drawing.SystemColors.Control;
            this.framesFlowLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.framesFlowLayoutPanel.Location = new System.Drawing.Point(3, 47);
            this.framesFlowLayoutPanel.Name = "framesFlowLayoutPanel";
            this.framesFlowLayoutPanel.Size = new System.Drawing.Size(556, 60);
            this.framesFlowLayoutPanel.TabIndex = 0;
            this.framesFlowLayoutPanel.WrapContents = false;
            // 
            // forwardButton
            // 
            this.forwardButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.forwardButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.forwardButton.BackgroundImage = global::B_trees.Properties.Resources.player_fwd;
            this.forwardButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.forwardButton.Location = new System.Drawing.Point(302, 13);
            this.forwardButton.Name = "forwardButton";
            this.forwardButton.Size = new System.Drawing.Size(28, 28);
            this.forwardButton.TabIndex = 2;
            this.forwardButton.UseVisualStyleBackColor = true;
            this.forwardButton.Click += new System.EventHandler(this.forwardButton_Click);
            // 
            // playButton
            // 
            this.playButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.playButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.playButton.BackgroundImage = global::B_trees.Properties.Resources.player_play;
            this.playButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.playButton.Location = new System.Drawing.Point(268, 13);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(28, 28);
            this.playButton.TabIndex = 1;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // toStartButton
            // 
            this.toStartButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.toStartButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.toStartButton.BackgroundImage = global::B_trees.Properties.Resources.player_start;
            this.toStartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toStartButton.Location = new System.Drawing.Point(200, 13);
            this.toStartButton.Name = "toStartButton";
            this.toStartButton.Size = new System.Drawing.Size(28, 28);
            this.toStartButton.TabIndex = 5;
            this.toStartButton.UseVisualStyleBackColor = true;
            this.toStartButton.Click += new System.EventHandler(this.toStartButton_Click);
            // 
            // workPanel
            // 
            this.workPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.workPanel.AutoScroll = true;
            this.workPanel.BackColor = System.Drawing.SystemColors.Control;
            this.workPanel.Location = new System.Drawing.Point(111, 86);
            this.workPanel.Name = "workPanel";
            this.workPanel.Size = new System.Drawing.Size(565, 250);
            this.workPanel.TabIndex = 4;
            this.workPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.workPanel_Paint);
            this.workPanel.Resize += new System.EventHandler(this.workPanel_Resize);
            // 
            // additionalDescriptionLabel
            // 
            this.additionalDescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.additionalDescriptionLabel.AutoSize = true;
            this.additionalDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.additionalDescriptionLabel.Location = new System.Drawing.Point(121, 63);
            this.additionalDescriptionLabel.Name = "additionalDescriptionLabel";
            this.additionalDescriptionLabel.Size = new System.Drawing.Size(34, 20);
            this.additionalDescriptionLabel.TabIndex = 1;
            this.additionalDescriptionLabel.Text = "-----";
            // 
            // mainActionLabel
            // 
            this.mainActionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainActionLabel.AutoSize = true;
            this.mainActionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mainActionLabel.Location = new System.Drawing.Point(118, 34);
            this.mainActionLabel.Name = "mainActionLabel";
            this.mainActionLabel.Size = new System.Drawing.Size(49, 29);
            this.mainActionLabel.TabIndex = 0;
            this.mainActionLabel.Text = "----";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 462);
            this.Controls.Add(this.navigationGroupBox);
            this.Controls.Add(this.actionGroupBox);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.mainActionLabel);
            this.Controls.Add(this.additionalDescriptionLabel);
            this.Controls.Add(this.workPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "MainForm";
            this.Text = "Візуальна реалізація дерев 2-3-4";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.actionGroupBox.ResumeLayout(false);
            this.actionGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.navigationGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem вихідToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem допомогаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проПрограмуToolStripMenuItem;
        private System.Windows.Forms.Button addRandomElemButton;
        private System.Windows.Forms.GroupBox actionGroupBox;
        private System.Windows.Forms.GroupBox navigationGroupBox;
        private System.Windows.Forms.Panel workPanel;
        private System.Windows.Forms.FlowLayoutPanel framesFlowLayoutPanel;
        private System.Windows.Forms.Button addElemButton;
        private System.Windows.Forms.Button findElemButton;
        private System.Windows.Forms.Button findRandomElemButton;
        private System.Windows.Forms.Button delElemВutton;
        private System.Windows.Forms.Button toEndButton;
        private System.Windows.Forms.Button rewindButton;
        private System.Windows.Forms.Button forwardButton;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button toStartButton;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button delRandomElemButton;
        private System.Windows.Forms.Label additionalDescriptionLabel;
        private System.Windows.Forms.Label mainActionLabel;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label1;
    }
}

