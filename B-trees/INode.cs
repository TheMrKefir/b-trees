﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace B_trees
{
    public interface INode// : ICloneable
    {
        Tree234Node[] Children { get; }
        Tree234Node Parent { get; }
        KeyValue<long, string>[] Pairs { get; }
    }
}
