﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Візуальна реалізація дерев 2-3-4")]
[assembly: AssemblyDescription("\r\n             \"Візуальна реалізація дерев 2-3-4\"\r\n               Виконав студент групи 427-ПОМ\r\n                           Капітоненко Сергій")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Промислово-економічний коледж НАУ")]
[assembly: AssemblyProduct("Візуальна реалізація дерев 2-3-4")]
[assembly: AssemblyCopyright("Капітоненко Сергій 2015 рік")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("53710b17-d66e-4dc2-bf37-511f59298545")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
