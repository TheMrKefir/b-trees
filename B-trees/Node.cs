﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace B_trees
{
    public struct ColorInt32 
    {
        public int Key;
        public Color KeyColor;

        public ColorInt32(int key, Color keyColor)
        {
            this.Key = key;
            this.KeyColor = keyColor;
        }
    }

    public partial class Node : Control 
    {
        int parent;
        int number;
        int k;
        int[] children;
        KeyValue<long, string>[] pairs;
        Color[] itemsColor;

        ToolTip t;
        int oldn;

        Pen p, pF;
        Brush b;

        Bitmap back;

        public int[] Children
        {
            get { return children; }
            set { children = value; }
        }

        public KeyValue<long, string>[] Pairs
        {
            get { return pairs; }
        }

        public Color[] ItemsColor
        {
            get { return itemsColor; }
            //set { itemsColor = value; }
        }

        public int Number
        {
            get { return number; }
            set { number = value; }
        }

        public int ParentNode
        {
            get { return parent; }
            set { parent = value; }
        }

        public int Level
        {
            get 
            {
                if (number == 0)
                    return 1;
                else if (number < 5)
                    return 2;
                else if (number < 21)
                    return 3;
                else
                    return 4; 
            }
        }

        public Point MidPoint
        {
            get
            {
                return new Point(Left + (Width / 2), Top);
            }
        }

        public Point[] ChildrenPoints
        {
            get
            {
                int h = Top + Height, k = Width / 3;

                return new Point[] {    new Point(Left, Top + Height),
                                        new Point(Left + k, h),
                                        new Point(Left + k + k, h),
                                        new Point(Left + Width, h)};
            }
        }

        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }

            set
            {
                base.ForeColor = value;

                p = new Pen(this.ForeColor, 3);

                pF = new Pen(value, 3);
                pF.EndCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
                pF.StartCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;

                b = new SolidBrush(this.ForeColor);

                NewBackground();
            }
        }

        public Pen ForegrountPen
        {
            get { return pF; }
        }

        private Node()
        {
            t = new ToolTip();
            oldn = -1;

            parent = -1;
            children = new int[4] { -1, -1, -1, -1 };
            pairs = new KeyValue<long, string>[3];
            itemsColor = new Color[3] { Color.Transparent, Color.Transparent, Color.Transparent };

            p = new Pen(this.ForeColor, 3);
            b = new SolidBrush(this.ForeColor);

            this.SizeChanged += Node_SizeChanged;

            Size = new Size(100, 30);

            pF = new Pen(this.ForeColor, 3);
            pF.EndCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
            pF.StartCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;

            InitializeComponent();
        }

        private void Node_SizeChanged(object sender, EventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.FillMode = System.Drawing.Drawing2D.FillMode.Winding;
            path.AddRectangle(new Rectangle(0, 10, Width, Height - 10));
            path.AddRectangle(new Rectangle(10, 0, Width - 20, 10));
            path.AddArc(0, 0, 20, 20, 180, 90);
            path.AddArc(this.Width - 21, 0, 20, 20, 270, 90);

            this.Region = new Region(path);

            k = this.Width / 3;

            NewBackground();
        }

        public void NewBackground()
        {
            back = new Bitmap(Width, Height);

            Graphics g = Graphics.FromImage(back);
            g.Clear(BackColor);

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Font f = new Font(this.Font.FontFamily, Height / 2.5F, FontStyle.Bold, GraphicsUnit.Pixel);

            int pairsCount = 0;

            for (int i = 0; i < 3; i++)
            {
                if (pairs[i] != null)
                    pairsCount++;
                else
                    break;
            }

            if (pairsCount != 0)
            {
                g.DrawLine(p, 0, 10, 0, this.Height - 1);
                g.DrawArc(p, 0, 0, 20, 20, 180, 90);

                g.DrawLine(p, 10, 0, this.Width - 11, 0);

                g.DrawLine(p, 0, this.Height - 1, this.Width - 1, this.Height - 1);

                g.DrawArc(p, this.Width - 21, 0, 20, 20, 270, 90);
                g.DrawLine(p, this.Width - 1, 10, this.Width - 1, this.Height - 1);

                g.DrawLine(p, k, 0, k, this.Height - 1);
                g.DrawLine(p, 2 * k, 0, 2 * k, this.Height - 1);

                for (int i = 0; i < 3; i++)
                    if (pairs[i] != null)
                    {
                        SizeF s = g.MeasureString(pairs[i].Key.ToString(), f);
                        if (itemsColor[i] == Color.Transparent)
                            g.DrawString(pairs[i].Key.ToString(), f, b, i * k + ((k - s.Width) / 2), (Height - s.Height) / 2);
                        else
                            g.DrawString(pairs[i].Key.ToString(), f, new SolidBrush(itemsColor[i]), i * k + ((k - s.Width) / 2), (Height - s.Height) / 2);
                    }
                    else
                        break;
            }
            else 
            {
                SizeF s = g.MeasureString("Дерево пусте", f);
                g.DrawString("Дерево пусте", f, b, (Width - s.Width) / 2, (Height - s.Height) / 2);
            }

                
            this.BackgroundImage = back;
        }

        public Node(Tree234Node p) : this()
        {
            number = p.Number;

            if (number != 0)
                parent = p.Parent.Number;

            p.Pairs.CopyTo(pairs, 0);

            this.ForeColor = Color.Black;
        }

        public Node(Tree234Node p, Color color) : this()
        {
            if (p == null)
                p = new Tree234Node();

            number = p.Number;

            if (number != 0)
                parent = p.Parent.Number;

            p.Pairs.CopyTo(pairs, 0);

            this.ForeColor = color;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void Node_MouseMove(object sender, MouseEventArgs e)
        {
            int n = (3 * e.X) / this.Width;

            if (n == oldn || n > 2)
                return;

            t.SetToolTip(this, pairs[n] == null ? "" : pairs[n].Value);

            oldn = n;
        }

        private void Node_MouseLeave(object sender, EventArgs e)
        {
            //t.Hide();
        }
    }
}
