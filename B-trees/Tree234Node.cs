﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B_trees
{
    public class KeyValue<TKey, TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public KeyValue(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }

    public class Tree234Node : INode
    {
        private Tree234Node[] children;
        private Tree234Node parent;
        private KeyValue<long, string>[] pairs;
        private int number;

        public int Number
        {
            get { return number; }
            set 
            { 
                number = value;

                for (int i = 0; i < 4; i++)
                    if (children[i] != null)
                    {
                        children[i].Number = (number * 4) + i + 1;
                    }
                    else
                        break;
            }
        }

        public int Count
        { 
            get
            {
                int i = 0;

                while(i < 3 && pairs[i] != null)
                        i++;

                return i; 
            }
        }
        public bool IsFull
        {
            get { return (this.Count == 3) ? true : false; }
        }
        public bool IsLeaf
        {
            get { return (children != null && children[0] == null) ? true : false; }
        }


        public Tree234Node this[int n]
        {
            get { return children[n]; }
        }
        public Tree234Node[] Children
        {
            get { return children; }
        }

        public Tree234Node Parent
        {
            get { return parent; }
            private set 
            {
                parent = value;
            }
        }

        public KeyValue<long, string>[] Pairs
        {
            get { return pairs; }
        }

        public Tree234Node()
        {
            number = 0;
            children = new Tree234Node[4];
            pairs = new KeyValue<long, string>[3];
        }

        public Tree234Node(KeyValue<long, string> pair, Tree234Node p = null) : this()
        {
            parent = p;

            pairs[0] = pair;
        }

        public string GetValue(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentException();

            return pairs[index].Value;
        }

        public KeyValue<long, string> GetKeyValuePair(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentException();

            return pairs[index];
        }

        public void ConnectChild(int childIndex, Tree234Node child)
        {
            children[childIndex] = child;
            if (child != null)
            {
                child.parent = this;

                child.Number = (number * 4) + childIndex + 1;
            }
        }

        public Tree234Node DisconnectChild(int childIndex)
        {
            Tree234Node temp = children[childIndex];
            children[childIndex] = null;

            return temp;
        }

        public int FindItem(long key)
        {
            if (pairs == null)
                return -1;

            for (int i = 0; i < 3; i++)
            {
                if (pairs[i] == null)
                    break;
                else if (pairs[i].Key == key)
                    return i;
            }

            return -1;
        }

        public int InsertItem(KeyValue<long, string> newItem)
        {
            if (this.Count == 3)
                throw new InvalidOperationException();

            for (int i = 2; i >= 0; i--)
            {
                if (pairs[i] == null)
                    continue;
                else
                {
                    if (pairs[i].Key > newItem.Key)
                        pairs[i + 1] = pairs[i];
                    else
                    {
                        pairs[i + 1] = newItem;
                        return i + 1;
                    }
                }
            }

            pairs[0] = newItem;

            return 0;
        }

        //видалення НАЙБІЛЬШОГО елемента
        public KeyValue<long, string> RemoveItem()
        {
            int count = this.Count;

            if (count == 0)
                throw new InvalidOperationException();

            KeyValue<long, string> temp = pairs[count - 1];
            pairs[count - 1] = null;

            return temp;
        }

        public KeyValue<long, string> RemovePair(int index) 
        {
            KeyValue<long, string> K = pairs[index];

            int count = this.Count;

            int i;
            for (i = index; i < count - 1; i++)
            {
                pairs[i] = pairs[i + 1];
            }

            pairs[i] = null;

            return K;
        }

        public KeyValue<long, string> Find(long key)
        {
            if (pairs == null)
                return null;

            for (int i = 0; i < 3; i++)
            {
                if (pairs[i] == null)
                    break;
                else if (pairs[i].Key == key)
                    return pairs[i];
                else if (pairs[i].Key > key)
                    children[i].Find(key);
            }

            return children[3].Find(key);
        }

        public override string ToString()
        {
            string buf = "( ";

            for (int i = 0; i < 3; i++)
            {
                if (pairs[i] != null)
                    buf += pairs[i].Key + " ";
                else
                    break;
            }

            return buf + ")";
        }
    }
}
