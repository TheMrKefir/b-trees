﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace B_trees
{
    public enum FrameType
    {
        Primary, Intermediate
    }

    public partial class Frame : UserControl
    {
        private FrameType type;

        public Node[] Nodes;

        public int Level;

        MainForm m;

        bool enable;

        public bool Enable
        {
            get { return enable; }
            set
            {
                if (value)
                    BackColor = Color.LightSteelBlue;
                else
                    BackColor = DefaultBackColor;

                enable = value;
            }
        }

        public FrameType Type
        {
            get { return type; }
            set
            {
                type = value;

                switch (value)
                {
                    case FrameType.Intermediate:
                        pictureBox.Image = global::B_trees.Properties.Resources.dot_small;
                        break;
                    case FrameType.Primary:
                        pictureBox.Image = global::B_trees.Properties.Resources.dot_big;
                        break;
                }
            }
        }

        override public string Text
        {
            get
            {
                return label.Text;
            }
            set
            {
                label.Text = value;
                this.Width = label.Width + 6;
            }
        }

        public string AdditionalText
        {
            get; set;
        }

        public Frame()
        {
            Nodes = new Node[((int)Math.Pow(4, Tree234.MaxLevel) - 1) / 3];

            InitializeComponent();

            this.SizeChanged += Frame_SizeChanged;

            Type = FrameType.Intermediate;
            this.Width = label.Width + 6;
        }

        private void Frame_SizeChanged(object sender, EventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();

            path.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;

            path.AddArc(0, 0, 10, 10, 180, 90);
            path.AddArc(Width - 10, 0, 10, 10, 270, 90);
            path.AddArc(Width - 10, Height - 10, 10, 10, 0, 90);
            path.AddArc(0, Height - 10, 10, 10, 90, 90);

            this.Region = new Region(path);
        }

        private void Frame_Click(object sender, EventArgs e)
        {
            m.Invoke(new System.Threading.ParameterizedThreadStart(m.ShowFrame), this);
        }

        private void Frame_ParentChanged(object sender, EventArgs e)
        {
            m = (MainForm)this.Parent.Parent.Parent;
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            m.Invoke(new System.Threading.ParameterizedThreadStart(m.ShowFrame), this);
        }

        private void label_Click(object sender, EventArgs e)
        {
            m.Invoke(new System.Threading.ParameterizedThreadStart(m.ShowFrame), this);
        }
    }
}
